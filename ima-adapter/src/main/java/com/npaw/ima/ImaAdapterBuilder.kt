package com.npaw.ima

import com.google.ads.interactivemedia.v3.api.AdsManager
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader
import com.google.android.exoplayer2.ext.ima.ImaServerSideAdInsertionMediaSource
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter


class ImaAdapterBuilder {

    private val _adsImaAdapterList = mutableListOf<ImaAdapter>()
    val adsImaAdapterList = _adsImaAdapterList.toList()

    private val _previousArgs = mutableListOf<Any>()

    /**
     *  `addArgs` accepts data as: ImaAdapter, ImaServerSideAdInsertionMediaSource.AdsLoader.Builder, ImaAdsLoader.Builder, AdsManager
     * **/
    fun addArg(data: Any) : ImaAdapterBuilder {
        if (_previousArgs.contains(data)){
            YouboraLog.error("Argument already added in ImaAdapterBuilder")
            return this
        }

        if(data is ImaAdapter){
            registerNewAdapter(data, data)
        }

        try {
            if(data is ImaServerSideAdInsertionMediaSource.AdsLoader.Builder) {
                val adapter = ImaAdapter()
                data.setAdErrorListener(adapter)
                data.setAdEventListener(adapter)
                adapter.setAdInsertionType(AdAdapter.InsertionType.ServerSide)
                registerNewAdapter(data, adapter)
            }
        } catch (_: ClassNotFoundException){ }
        catch (_: NoClassDefFoundError) {}

        try {
            if(data is ImaAdsLoader.Builder) {
                val adapter = ImaAdapter()
                data.setAdErrorListener(adapter)
                data.setAdEventListener(adapter)
                adapter.setAdInsertionType(AdAdapter.InsertionType.ClientSide)
                registerNewAdapter(data, adapter)
            }
        } catch (_: ClassNotFoundException){ }
        catch (_: NoClassDefFoundError) {}

        if(data is AdsManager) {
            registerNewAdapter(data, ImaAdapter(data))
        }

        if(!_previousArgs.contains(data)){
            _previousArgs.add(data)
            YouboraLog.error("Argument $data not compatible with ImaAdapterBuilder")
        }
        return this
    }

    private fun registerNewAdapter(data: Any, newAdapter: ImaAdapter){
        _previousArgs.add(newAdapter)

        if(!_previousArgs.contains(data))
            _previousArgs.add(data)

        _adsImaAdapterList.add(newAdapter)
    }

    /**
     *
     *
     *
     * **/
    fun build() : AdAdapter<*> {
        val instanceAdapter = when (_adsImaAdapterList.size) {
            0 -> ImaAdapter(null)
            1 -> _adsImaAdapterList[0]
            else -> ImaAdapterHandler(_adsImaAdapterList)
        }

        _adsImaAdapterList.clear()
        _previousArgs.clear()
        return instanceAdapter
    }
}