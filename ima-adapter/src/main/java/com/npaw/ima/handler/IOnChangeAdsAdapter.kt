package com.npaw.ima.handler

import com.npaw.youbora.lib6.adapter.AdAdapter

interface IOnChangeAdsAdapter {
    fun setCurrentAdsAdapter(adsAdapter: AdAdapter<*>)
}