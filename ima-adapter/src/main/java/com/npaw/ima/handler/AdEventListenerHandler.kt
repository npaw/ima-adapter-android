package com.npaw.ima.handler

import com.npaw.youbora.lib6.adapter.AdAdapter

class AdEventListenerHandler(val adsAdapter: AdAdapter<*>,val onChangeListener: IOnChangeAdsAdapter) : AdAdapter.AdAdapterEventListener {
    override fun onClick(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onAdInit(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onQuartile(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onManifest(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onAdBreakStart(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onAdBreakStop(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onStart(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onJoin(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onPause(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onResume(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onStop(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onBufferBegin(convertFromSeek: Boolean, params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onBufferEnd(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }

    override fun onError(params: MutableMap<String, String>) {
        onChangeListener.setCurrentAdsAdapter(adsAdapter)
    }
}