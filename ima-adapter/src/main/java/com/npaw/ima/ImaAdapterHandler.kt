package com.npaw.ima

import com.google.ads.interactivemedia.v3.api.*
import com.npaw.ima.handler.AdEventListenerHandler
import com.npaw.ima.handler.IOnChangeAdsAdapter
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter
import com.npaw.youbora.lib6.adapter.PlaybackChronos
import com.npaw.youbora.lib6.adapter.PlayheadMonitor
import com.npaw.youbora.lib6.flags.AdFlags
import com.npaw.youbora.lib6.flags.BaseFlags
import com.npaw.youbora.lib6.flags.Flags
import java.util.HashMap

open class ImaAdapterHandler(adAdapterList: List<AdAdapter<*>>) : AdAdapter<BaseManager>(null), IOnChangeAdsAdapter {

    private val _adAdapterList: List<AdAdapter<*>> = adAdapterList.toMutableList()
    init {
        adAdapterList.forEach { adAdapter ->
            adAdapter.plugin = this.plugin
            adAdapter.addEventListener(AdEventListenerHandler(adAdapter, this))
        }
    }

    val adAdapterList: List<AdAdapter<*>> = _adAdapterList.toList()

    private var _currentAdsAdapter : AdAdapter<*>? = null


    override fun setCurrentAdsAdapter(adsAdapter: AdAdapter<*>) {
        if(!_adAdapterList.contains(adsAdapter)) {
            YouboraLog.error("AdsAdapter not registered.")
            return
        }
        _currentAdsAdapter = adsAdapter
    }

    /** BaseAdapter **/
    override var monitor: PlayheadMonitor? = null
        get() {
            return _currentAdsAdapter?.monitor?:field
        }
    
    override var flags = BaseFlags()
        get() {
            return _currentAdsAdapter?.flags?:field
        }

    override var chronos = PlaybackChronos()
        get() {
            return _currentAdsAdapter?.chronos?:field
        }

    override fun monitorPlayhead(monitorBuffers: Boolean, monitorSeeks: Boolean, interval: Int) {
        _currentAdsAdapter?.monitorPlayhead(monitorBuffers, monitorSeeks, interval)
    }

    /** BaseAdAdapter **/
    override fun getGivenBreaks(): Int? {
        return _currentAdsAdapter?.getGivenAds()
    }

    override fun addEventListener(eventListener: AdapterEventListener) {
        adAdapterList.forEach {
            it.plugin = plugin
            it.addEventListener(eventListener)
        }

    }

    /** Functions **/

    override fun getBreaksTime(): MutableList<*>? {
        return _currentAdsAdapter?.getBreaksTime()
    }

    override fun getPosition(): AdPosition {
        return _currentAdsAdapter?.getPosition()?:AdPosition.UNKNOWN
    }


    override fun getAdInsertionType(): String? {
        return _currentAdsAdapter?.getAdInsertionType()
    }

    override fun getGivenAds(): Int? {
        return _currentAdsAdapter?.getGivenAds()
    }
    override fun getAdProvider(): String? { return _currentAdsAdapter?.getAdProvider() }
    override fun getPlayerName(): String? { return "IMA" }
    override fun getTitle(): String? { return _currentAdsAdapter?.getTitle() }
    override fun getDuration(): Double? { return _currentAdsAdapter?.getDuration() }
    override fun getAdCreativeId(): String? { return _currentAdsAdapter?.getAdCreativeId() }
    override fun getIsAdSkippable(): Boolean? { return _currentAdsAdapter?.getIsAdSkippable()  }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName()  }
    override fun getPlayhead(): Double? { return  _currentAdsAdapter?.getPlayhead() }

    override fun fireStop(params: MutableMap<String, String>) {
        adAdapterList.forEach {
            it.fireStop(params)
        }
    }

    override fun fireAdInit(params: MutableMap<String, String>) {
        _currentAdsAdapter?.let{it.fireAdInit(params)}?: kotlin.run { YouboraLog.error("fireAdInit has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireClick(params: MutableMap<String, String>) {
        _currentAdsAdapter?.let{it.fireClick(params)}?: kotlin.run { YouboraLog.error("fireClick has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireClick(adUrl: String?) {
        _currentAdsAdapter?.let{it.fireClick(adUrl)}?: kotlin.run { YouboraLog.error("fireClick has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireSkip() {
        _currentAdsAdapter?.let{it.fireSkip()}?: kotlin.run { YouboraLog.error("fireSkip has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireQuartile(quartile: Int) {
        _currentAdsAdapter?.let{it.fireQuartile(quartile)}?: kotlin.run { YouboraLog.error("fireQuartile has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireManifest(params: MutableMap<String, String>) {
        _currentAdsAdapter?.let{it.fireManifest(params)}?: kotlin.run { YouboraLog.error("fireManifest has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireAdBreakStart(params: MutableMap<String, String>) {
        _currentAdsAdapter?.let{it.fireAdBreakStart(params)}?: kotlin.run { YouboraLog.error("fireAdBreakStart has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

    override fun fireAdBreakStop(params: MutableMap<String, String>) {
        _currentAdsAdapter?.let{it.fireAdBreakStop(params)}?: kotlin.run { YouboraLog.error("fireAdBreakStop has been called in the ImaAdapterHandler before setCurrentAdsAdapter") }
    }

}

