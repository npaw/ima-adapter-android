package com.npaw.ima

import com.google.ads.interactivemedia.v3.api.*
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter
import com.npaw.youbora.lib6.adapter.AdAdapter.InsertionType.ClientSide
import com.npaw.youbora.lib6.adapter.AdAdapter.InsertionType.ServerSide
import com.npaw.youbora.lib6.constants.RequestParams.AD_PLAYHEAD

import kotlin.math.roundToInt

open class ImaAdapter(loader: AdsLoader? = null) : AdAdapter<BaseManager>(null),
    AdEvent.AdEventListener, AdErrorEvent.AdErrorListener {

    private var currentAd: Ad? = null
    private var adsLoader = loader
    private var lastPosition: AdPosition? = null
    private var totalAds = super.getGivenAds()
    private var fireStartAfterPostRoll = false

    @JvmField
    var adInsertionType : String? = null

    private val adsLoadedListener = AdsLoader.AdsLoadedListener { event ->
        event.adsManager?.let {
            player = it
        } ?: event.streamManager?.let {
            player = it
        }
    }

    init {
        adsLoader?.let { adLoader ->
            adLoader.addAdsLoadedListener(adsLoadedListener)
            adLoader.addAdErrorListener(this)
        }
    }

    constructor(adsManager: AdsManager) : this(null) {
        player = adsManager
    }

    override fun registerListeners() {
        super.registerListeners()

        player?.let { p ->
            p.addAdErrorListener(this)
            p.addAdEventListener(this)
        }
    }

    override fun onAdEvent(adEvent: AdEvent?) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"OnAdEvent: $adEvent")
        adEvent?.let { event ->
            if (event.type != AdEvent.AdEventType.AD_PROGRESS) YouboraLog.requestLog(event.type.toString())
            currentAd = event.ad

            when (event.type) {
                AdEvent.AdEventType.LOADED -> {
                    currentAd?.let {
                        totalAds = it.adPodInfo.totalAds
                    }

                    if (player is StreamManager || getAdFlags().isAdBreakStarted)
                        fireStart()
                    else
                        null
                }
                AdEvent.AdEventType.AD_BREAK_STARTED -> {
                    if(!getAdFlags().isAdInitiated || !getAdFlags().isAdBreakStarted){
                        fireAdBreakStart()
                    }else
                        null
                }
                AdEvent.AdEventType.CONTENT_PAUSE_REQUESTED -> {
                    plugin?.adapter?.firePause()

                    if (player == null || player is AdsManager)
                        fireStart()
                    else
                        null
                }
                AdEvent.AdEventType.AD_BREAK_STARTED -> {
                    if(!getAdFlags().isAdInitiated || !getAdFlags().isAdBreakStarted){
                        fireStart()
                    } else {
                        null
                    }
                }
                AdEvent.AdEventType.AD_PROGRESS -> {
                    if (flags.isStarted && !flags.isJoined) {
                        fireJoin()
                    } else if (flags.isStarted && flags.isJoined) {
                        fireResume()
                        fireBufferEnd()
                    } else {
                        null
                    }
                }

                AdEvent.AdEventType.STARTED -> fireJoin()
                AdEvent.AdEventType.PAUSED -> firePause()
                AdEvent.AdEventType.RESUMED -> {
                    if (fireStartAfterPostRoll && getPosition() == AdPosition.PRE) fireStart()
                    fireStartAfterPostRoll = false
                    fireResume()
                }
                AdEvent.AdEventType.CLICKED -> fireClick(getUrl())
                AdEvent.AdEventType.CONTENT_RESUME_REQUESTED -> {
                    completed()
                    plugin?.adapter?.fireResume()
                }

                AdEvent.AdEventType.COMPLETED -> completed()
                AdEvent.AdEventType.SKIPPED -> fireSkip()
                AdEvent.AdEventType.AD_BREAK_ENDED -> fireAdBreakStop()
                AdEvent.AdEventType.ALL_ADS_COMPLETED -> fireAdBreakStop()
                AdEvent.AdEventType.FIRST_QUARTILE -> fireQuartile(1)
                AdEvent.AdEventType.MIDPOINT -> fireQuartile(2)
                AdEvent.AdEventType.THIRD_QUARTILE -> fireQuartile(3)
                AdEvent.AdEventType.AD_BUFFERING -> fireBufferBegin()
                AdEvent.AdEventType.LOG -> {
                    event.adData["errorCode"]?.let { errorCode ->
                        if (errorCode != "1009") {
                            fireError(event.adData["errorCode"], event.adData["errorMessage"],
                                null)
                        }
                    }
                }

                else -> null
            }
        }
    }

    private fun completed() {
        if (lastPosition != AdPosition.POST)
            lastPosition = getPosition()
        else
            fireStartAfterPostRoll = true

        // IMA "resets" playhead before firing this event so we send duration instead
        fireStop(HashMap<String, String>().apply {
            put(AD_PLAYHEAD, (getDuration() ?: 0.0 ).toString())
        })
    }

    override fun onAdError(adErrorEvent: AdErrorEvent?) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"onAdError: $adErrorEvent")
        val error = adErrorEvent?.error
        val message = error?.message
        val code = error?.errorCode

        if (flags.isStarted) {
            fireError(code?.errorNumber.toString(), message)
        } else {
            when (code) {
                AdError.AdErrorCode.ADS_REQUEST_NETWORK_ERROR,
                AdError.AdErrorCode.VAST_LOAD_TIMEOUT -> {
                    fireManifest(ManifestError.NO_RESPONSE, message)
                }

                AdError.AdErrorCode.VAST_EMPTY_RESPONSE -> {
                    fireManifest(ManifestError.EMPTY_RESPONSE, message)
                }

                AdError.AdErrorCode.VAST_MALFORMED_RESPONSE,
                AdError.AdErrorCode.UNKNOWN_AD_RESPONSE -> {
                    fireManifest(ManifestError.WRONG_RESPONSE, message)
                }
                else -> {}
            }
        }
    }

    private fun getUrl(): String? {
        var url: String? = null

        currentAd?.let { ad ->
            val f = ad.javaClass.getDeclaredField("clickThroughUrl")
            f.isAccessible = true
            url = f.get(ad)?.toString()
        }

        return url
    }

    override fun unregisterListeners() {
        super.unregisterListeners()

        player?.let { p ->
            p.removeAdErrorListener(this)
            p.removeAdEventListener(this)
        }

        adsLoader?.let { adLoader ->
            adLoader.removeAdErrorListener(this)
            adLoader.removeAdsLoadedListener(adsLoadedListener)
        }
    }

    override fun getGivenBreaks(): Int? {
        var givenBreaks = super.getGivenBreaks()

        player?.let { p ->
            if (p is AdsManager) {
                givenBreaks = p.adCuePoints.size
            } else if (p is StreamManager) {
                givenBreaks = p.cuePoints.size
            }
        }

        return givenBreaks
    }

    override fun addEventListener(eventListener: AdapterEventListener) {
            super.addEventListener(eventListener)
    }

    override fun getBreaksTime(): MutableList<*>? {
        var breaksTime = super.getBreaksTime()

        if (player != null) {

            if (player is AdsManager) {
                breaksTime = (player as AdsManager).adCuePoints
            } else if (player is StreamManager) {
                breaksTime = ArrayList<Double>()

                for (point in (player as StreamManager).cuePoints)
                    breaksTime.add(point.startTimeMs / 1000.0 )
            }
        }

        return breaksTime
    }

    override fun getPosition(): AdPosition {
        var position: AdPosition = AdPosition.UNKNOWN

        currentAd?.adPodInfo?.let { podInfo ->
            val pos = podInfo.podIndex

            when {
                pos == 0 -> position = AdPosition.PRE
                pos == -1 -> position = AdPosition.POST
                pos > 0 -> position = AdPosition.MID
            }
        }

        player?.let { p ->
            if (p is StreamManager) position = getDAIPosition()
        }

        return position
    }

    private fun getDAIPosition(): AdPosition {
        var position = AdPosition.UNKNOWN

        plugin?.takeIf { it.isLive }?.let { return AdPosition.MID }

        currentAd?.let { ad ->
            val offset = ad.adPodInfo.timeOffset

            if (offset == 0.0) {
                position = AdPosition.PRE
            } else {
                plugin?.adapter?.getDuration()?.let { adapterDuration ->
                    getDuration()?.takeIf {
                        offset == (adapterDuration.roundToInt() - it.roundToInt()).toDouble()
                    }?.let { position = AdPosition.POST }
                }
            }
        }

        return position
    }


    fun setAdInsertionType(type: String?){
        adInsertionType = type
    }

    override fun getAdInsertionType(): String? {
        return adInsertionType?:if (player is StreamManager)
            ServerSide
        else
            ClientSide
    }

    override fun getGivenAds(): Int? { return totalAds }
    override fun getAdProvider(): String { return "DFP" }
    override fun getPlayerName(): String { return "IMA" }
    override fun getTitle(): String? { return currentAd?.title }
    override fun getDuration(): Double? { return currentAd?.duration }
    override fun getAdCreativeId(): String? { return currentAd?.creativeId }
    override fun getIsAdSkippable(): Boolean? { return currentAd?.isSkippable }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun getPlayhead(): Double? {
        return player?.adProgressInfo?.currentTime ?: (chronos.total.getDeltaTime(false) / 1000.0)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun resetValues() {
        totalAds = super.getGivenAds()
        currentAd = null
    }
}
