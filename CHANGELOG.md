## [6.8.4] - 2023-09-16
### Added
- ima-adapter: Add support for buffering in ads.

## [6.8.3] - 2023-09-16
### Updated
- demo-exoplayer-csai+ssai: Update exoplayer version to `2.19.1`

## [6.8.2] - 2023-03-16
### Updated
- Update lib version to `6.8.22`
### Added
- Support to IMA SDK to `3.30.3`


## [6.8.1] - 2023-03-16
### Added
- ImaAdapterBuilder
- ImaAdapterHandler: handler/proxy for multiples adsAdapters for the same player
- Support for ImaServerSideAdInsertionMediaSource using ImaAdapterBuilder
- Added setAdInsertionType
- Demo CSAI + SSAI
### Updated
- Update lib version to `6.8.11`
### Fixed
- Demos

## [6.8.0] - 2022-10-28
### Updated
- Update lib version to `6.8.3`
- Support for IMA SDK `3.29.0`

## [6.7.21] - 2022-07-05
### Fixed
- Avoid triggering `joinTime` & `fireResume` in `AdEvent.AdEventType.AD_PROGRESS` when ad is joined.
- ### Updated
- Update lib version to `6.7.69`

## [6.7.20] - 2022-05-31
### Fixed
- Getting `AdPlayback` from `chronos.total.getDeltaTime` if AdLoader is not providing a value.

## [6.7.19] - 2022-05-26
### Fixed
- Trigger `fireStart()` if pre-roll begins and `lastPosition` was post-roll.
- ### Updated
- Update lib version to `6.7.68`

## [6.7.18] - 2022-04-11
### Added
- Support for IMA SDK `3.27.0`
### Updated
- Update lib version to `6.7.64`
- Update demo.
### Removed
- Remove demo deprecated references.

## [6.7.17] - 2022-03-08
### Changed
- Content resume fired when `CONTENT_RESUME_REQUESTED`.

## [6.7.16] - 2022-03-07
### Changed
- Useless code removed

## [6.7.15] - 2022-03-07
### Fixed
- Resume not being triggered when `CONTENT_RESUME_REQUESTED`.

## [6.7.14] - 2022-02-04
### Fixed
- Ad not resuming after ad click.
- `fireStart()` triggered too son for IMA Extension.

## [6.7.13] - 2021-12-03
### Added
- Support for the latest version of IMA Extension.

## [6.7.12] - 2021-05-26
### Added
- Ad insertion type.

## [6.7.11] - 2021-05-12
### Fixed
- Ad playhead.

## [6.7.10] - 2021-03-18
### Modified
- Deployment platform, moved from Bintray to JFrog.

## [6.7.9] - 2020-11-24
### Fixed
- fireStart() not sent when several ads on the same IMA break.

## [6.7.8] - 2020-10-20
### Removed
- AD_BUFFERING event.

## [6.7.7] - 2020-10-08
### Fixed
- fireBufferBegin() triggered right after fireBufferEnd().

## [6.7.6] - 2020-07-31
### Fixed
- fireStart() triggered too soon on IMA.
### Removed
- Ton of new logs.

## [6.7.5] - 2020-07-28
### Added
- Ton of new logs.

## [6.7.4] - 2020-07-23
### Added
- fireJoin() called when AD_PROGRESS.
 
## [6.7.3] - 2020-03-04
### Updated
- Youboralib version

## [6.7.2] - 2020-02-21
### Added
- ImaAdapter(AdsManager) constructor.

## [6.7.1] - 2020-02-20
### Fixed
- adBreakStart & adBreakStop.

## [6.7.0] - 2020-02-18
### Refactored
- Adapter to work with the new Youboralib version.

### Removed
- ImaDAIAdapter.
- ImaAdapter(AdsManager) constructor.

## [6.5.0] - 2019-06-19
### Added
- Ads rework.
- All adapters unified in one (ImaAdapter).
### Deprecated 
- ImaAdapter(AdsLoader) constructor.
- ImaDAIAdapter.

## [6.4.1] - 2019-04-29
### Fixed
- Content not paused when an ad starts (IMA extension).
- DAI ads position won't be shown as pre on live anymore.
- getPosition() crash when duration is null (DAI).

## [6.4.0] - 2019-04-16
### Added
- Youboralib version updated to version `6.4.1`

## [6.3.0] - 2019-02-01
### Added
- Youboralib version updated to `6.3.6`

## [6.0.11] - 2018-12-20
### Fixed
- Removed unused event, allAdsCompleted.

## [6.0.10] - 2018-11-14
### Fixed
- Fixed null pointer exception in case of using the loader and calling fireAdInit

## [6.0.9] - 2018-07-31
### Fixed
- Now when AdsManager/StreamManager is null, the application won't crash anymore.

## [6.0.8] - 2018-05-24
### Added
 - Sample project for IMA DAI
### Fixed
 - Now for live ads all is considered midroll

## [6.0.7] 2018-04-23
### Removed
 - Ad pause / Ad resume are not send anymore on DAI to keep it consistent with iOS

## [6.0.6] 2018-04-17
### Fixed
 - Join time was not send in some cases

## [6.0.5] 2018-04-16
### Added
 - Support for DAI

## [6.0.4] 2018-03-15
### Added
 - Support for ExoPlayer IMA extension (for more information check documentation)

## [6.0.3] - 2017-12-15
### Added
 - Ad served metric
 - Ad Adapter version
 - FireAllAdsCompleted call
 - Stop if all ads completed
 
## [6.0.1] - 2017-10-10
### Added
 - AdInit added
 
## [6.0.0] - 2017-10-10
### Added
 - Release version
